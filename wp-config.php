<?php
/** 
 * Configuración básica de WordPress.
 *
 * Este archivo contiene las siguientes configuraciones: ajustes de MySQL, prefijo de tablas,
 * claves secretas, idioma de WordPress y ABSPATH. Para obtener más información,
 * visita la página del Codex{@link http://codex.wordpress.org/Editing_wp-config.php Editing
 * wp-config.php} . Los ajustes de MySQL te los proporcionará tu proveedor de alojamiento web.
 *
 * This file is used by the wp-config.php creation script during the
 * installation. You don't have to use the web site, you can just copy this file
 * to "wp-config.php" and fill in the values.
 *
 * @package WordPress
 */

// ** Ajustes de MySQL. Solicita estos datos a tu proveedor de alojamiento web. ** //
/** El nombre de tu base de datos de WordPress */
define('DB_NAME', 'Blog');

/** Tu nombre de usuario de MySQL */
define('DB_USER', 'root');

/** Tu contraseña de MySQL */
define('DB_PASSWORD', '');

/** Host de MySQL (es muy probable que no necesites cambiarlo) */
define('DB_HOST', 'localhost:8080/phpmyadmin');

/** Codificación de caracteres para la base de datos. */
define('DB_CHARSET', 'utf8mb4');

/** Cotejamiento de la base de datos. No lo modifiques si tienes dudas. */
define('DB_COLLATE', '');

/**#@+
 * Claves únicas de autentificación.
 *
 * Define cada clave secreta con una frase aleatoria distinta.
 * Puedes generarlas usando el {@link https://api.wordpress.org/secret-key/1.1/salt/ servicio de claves secretas de WordPress}
 * Puedes cambiar las claves en cualquier momento para invalidar todas las cookies existentes. Esto forzará a todos los usuarios a volver a hacer login.
 *
 * @since 2.6.0
 */
define('AUTH_KEY', 'y&w}:oLCOfQCiQ_hP3ng_$rK,XhyFB2QS9Tq!yOzrMr5^xZJ^Xw;Xd:uVShz_#)x');
define('SECURE_AUTH_KEY', 't?fhX$xL9>V$SY]!Ef[,R|R`#s.gHb76sQ%N@T@nmnV^j-#n/|Ys)kF9_2rZ,7jz');
define('LOGGED_IN_KEY', '[0iMS7R>13hYK9>jABbwndhIVo|:Oi%eW23jnhUNX2VXPDIoO[?~L?kihf> )G6/');
define('NONCE_KEY', 'SEYv/FQzk!gMn=YRq:8r[Hs}=BnoM#XwtpwZCmG}>q~Sk9Fg>0HGNd<1/9@?WAD7');
define('AUTH_SALT', '>fBK?kB26k3*TF^3Z|`rd%L#<l36udL6pt%M:|EPoLz}ZgFpjmWCD?hfCpRrkUtZ');
define('SECURE_AUTH_SALT', 'A2IlH}8=J4)?:j4qMNKq&Y6q!|e@W!FG;+V{0K)%([)ILn!4n8Gr~Hvb|9yi `;(');
define('LOGGED_IN_SALT', 'eP9+nsjO$=8U/^_T-8&)4)tpncqkJ;&J|u.6T/,rT{;8dR@2hC?T:rBJX%F>.vN>');
define('NONCE_SALT', ':2,tl0rBl.]+-wfH*h_hS63EL:eEzxobF!|,uPCmQwjpiiWM3Sah7V(lqP{ <|Kn');

/**#@-*/

/**
 * Prefijo de la base de datos de WordPress.
 *
 * Cambia el prefijo si deseas instalar multiples blogs en una sola base de datos.
 * Emplea solo números, letras y guión bajo.
 */
$table_prefix  = 'wp_';


/**
 * Para desarrolladores: modo debug de WordPress.
 *
 * Cambia esto a true para activar la muestra de avisos durante el desarrollo.
 * Se recomienda encarecidamente a los desarrolladores de temas y plugins que usen WP_DEBUG
 * en sus entornos de desarrollo.
 */
define('WP_DEBUG', false);

/* ¡Eso es todo, deja de editar! Feliz blogging */

/** WordPress absolute path to the Wordpress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
